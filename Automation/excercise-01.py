import array

def insert_element(arr, x, k):
    arr.insert(k, x)


my_array = [1, 2, 3, 4, 5]
insert_element(my_array, 10, 2)
print(my_array)  

def is_prime(n):
    if n <= 1:
        return False
    for i in range(2, int(n**0.5) + 1):
        if n % i == 0:
            return False
    return True

def remove_primes(arr):
    arr[:] = [x for x in arr if not is_prime(x)]

my_array02 = [1, 2, 3, 4, 5, 6, 7, 8, 9]
remove_primes(my_array02)
print(my_array02)  
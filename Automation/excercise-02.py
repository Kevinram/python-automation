class Node:
    def __init__(self, value):
        self.value = value
        self.next = None

class LinkedList:
    def __init__(self):
        self.head = None

    def add_node(self, value):
        new_node = Node(value)
        if self.head is None:
            self.head = new_node
        else:
            current = self.head
            while current.next:
                current = current.next
            current.next = new_node

    def print_list(self):
        current = self.head
        while current:
            print(current.value, end=" ")
            current = current.next
        print()


my_list = LinkedList()
my_list.add_node(10)
my_list.add_node(30)
my_list.add_node(50)
my_list.add_node(70)
my_list.add_node(90)


# Thêm phần tử 60 vào giữa danh sách (sau phần tử có giá trị 50)
new_node = Node(60)
current = my_list.head
while current:
    if current.value == 50:
        new_node.next = current.next
        current.next = new_node
        break
    current = current.next

# Thêm phần tử 100 vào cuối danh sách
my_list.add_node(100)

# In danh sách liên kết sau khi thêm các phần tử
#my_list.print_list()

# Phương thức clear để xóa tất cả các phần tử trong danh sách
my_list.head = None

# Hoặc sử dụng phương thức clear để xóa tất cả các phần tử
# def clear(self):
#     self.head = None

# In danh sách liên kết sau khi xóa
my_list.print_list()